
var $x1, $y1, $x2, $y2, $w, $h;

function selectChange(img, selection) {
    $x1.text(selection.x1);
    $y1.text(selection.y1);
    $x2.text(selection.x2);
    $y2.text(selection.y2);
    $w.text(selection.width);
    $h.text(selection.height);
}


$(document).ready(function () {
   //$("#color_picker").farbtastic("#box_color");
   var farb = $.farbtastic("#color_picker");
   $("#color_setting input").each(function () {
				      // set the background colors of all of the textfields appropriately
				      farb.linkTo(this);
				      // update the farbtastic colorpicker when this textfield is clicked
				      $(this).click(function () {
							farb.linkTo(this);
						    });
				  });


   $("#color_setting input#border_color").val("<none>");
   $("#color_setting input#background_color").val("<none>");

    //pdfstamper
    $("input[@name = 'page']").bind('click',function() {
	if($("input[@name = 'page']:checked").val()=="All"){
            $("input[@name = 'type2']").show();
            $("input[@name = 'type2']").attr({'disabled':'TRUE'});
        }
        if($("input[@name = 'page']:checked").val()=="Pages"){
            $("input[@name = 'type2']").show();
        }
    });

    $x1 = $('#x1');
    $y1 = $('#y1');
    $x2 = $('#x2');
    $y2 = $('#y2');
    $w = $('#w');
    $h = $('#h');
    $("input[@name='coordinates']").hide();

    //page setting
    $("#page_setting").hide();
    if($("input[@name = 'page']:checked").val() == "Pages") {
	$('#page_setting').show();
    }
    $("input[@name = 'page']").bind('click',(function() {
	if($("input[@name = 'page']:checked").val() == "Pages"){
	    $('#page_setting').show();
	}
	else {
	      $("#page_setting").hide();
	     }
    }));

});

//The below code uses imgAreaSelect 0.4.2 (http://odyniec.net/projects/imgareaselect) - Dual licensed under the MIT (MIT-LICENSE.txt)
//and GPL (GPL-LICENSE.txt) licenses.
$(window).load(function () {
    $('.image_container #preview-image').imgAreaSelect({ selectionColor: 'blue', onSelectChange: selectChange });
});


function populateCoordinates() {
    $("#x1h").val($("#x1").text());
    $("#y1h").val($("#y1").text());
    $("#x2h").val($("#x2").text());
    $("#y2h").val($("#y2").text());
    $("#wh").val($("#w").text());
    $("#hh").val($("#h").text());
};
