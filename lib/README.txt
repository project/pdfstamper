This is the location for the external libraries
 modules
   |
   |
   |-- pdfstamper
          |
          |--- pdfstamper.info
          |--- pdfstamper.module
          |--- pdfstamper.install
          |--- pdfstamper.inc
          |--- pdfstamper.css
          |--- pdf-preview.png
          |--- stamp.jpg
          |--- README.txt
          |--- js
          |     |
          |     |-- pdfstamper.js
          |     |
          |     |-- imgareaselect-0.4.2
          |     |        |
          |     |        |--- GPL-LICENSE.txt
          |     |        |--- jquery.imgareaselect-0.4.2.js
          |     |        |--- jquery.imgareaselect-0.4.2.min.js
          |     |        |--- MIT-LICENSE.txt
          |     |
          |     |-- jquery.imageareaselect.js -> imgareaselect-0.4.2/jquery.imgareaselect-0.4.2.min.js(create symlink manually)
          |
          |--- lib
                |
                |--  FPDF
                |      |
                |      |--- doc 
                |      |--- FAQ.htm 
                |      |--- font 
                |      |--- fpdf.css 
                |      |--- fpdf.php 
                |      |--- histo.htm 
                |      |--- install.txt 
                |      |--- tutorial 
                |
                |--  FPDF_TPL
                |      |
                |      |--- fpdf_tpl.php
                | 
                |--  FPDI
                       |
                       |--- decoders
                       |--- fpdi_pdf_parser.php
                       |--- fpdi.php
                       |--- pdf_context.php
                       |--- pdf_parser.php
                       |--- wrapper_functions.php

