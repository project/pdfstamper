<?php

  /**
   * @file
   * PDF Stamping Library
   */


  /*
   * CONSTANTS
   *
   */
define('PDFSTAMPER_MIN_FONT_SIZE', 8);
define('PDFSTAMPER_MAX_FONT_SIZE', 26);
define('PDFSTAMPER_VALID_COLOR_PATTERN', '/^#[0-9A-F]{6,6}$/i');
define('PDFSTAMPER_HEX2RGB_PATTERN', '/^#(.{2,2})(.{2,2})(.{2,2})$/');

/**
 * This is the main stamping function of the PDF Stamper library.
 *
 * It checks all parameters before it can proceed with the stamping. Once
 * the parameter checking is completed the stamping is completed and the
 * stamped document is sent to the end user.
 *
 * @param $pdf_files
 *   A string or an array of filenames
 *
 * @return
 *   TRUE if stamping was succesful, FALSE otherwise
 */
function pdfstamper_stamp($pdf_files) {

  /**
   * check $pdf_files as
   *   - is it an array or a string
   *   - do the values consist of valid filenames
   *   - do the files exist in the file repository
   * if all the above are ok lets proceed to the next parameter
   */
  if (!_pdfstamper_check_pdffile_param($pdf_files)) {
    return FALSE;
  }
  // we've completed checking for the pdf_files param, lets move onto the next parameter
} // end of function pdfstamper_stamp

function get_alignment_options() {
  $alignment_options = array(
    'J' => 'Justified',
    'R' => 'Right',
    'C' => 'Center',
    'L' => 'Left',
    );
  return $alignment_options;
}


/**
 * This function returns the valid font sizes for stamping
 *
 */
function get_font_size_options() {
  $font_size = array();
  foreach (range(PDFSTAMPER_MIN_FONT_SIZE, PDFSTAMPER_MAX_FONT_SIZE) as $v) {
    $font_size[$v] = t("$v");
  }
  return $font_size;
}
// end of function get_font_size_options


/*
 * Get valid font families
 */
function get_font_family_options() {
  $font_family = array(
    'Courier'      => t('Courier'),
    'Helvetica'    => t('Helvetica'),
    'Times'        => t('Times'),
    'Symbol'       => t('Symbol'),
    'ZapfDingbats' => t('ZapfDingbats'),
    );
  return $font_family;
}

/*
 * Get valid font styles
 */
function get_font_style_options() {
  $font_style = array(
    'B' => t('Bold'),
    'I' => t('Italic'),
    'U' => t('Underline'),
                      );
  return $font_style;
}


/*
 * Get valid pages
 */
function get_pages_options() {
  $_pages = array(
    'All'    => t('All'),
    'Pages'  => t('Pages'),
    );
  return $_pages;
}

function get_border_color_options(){
  $border_color_options = array(
    'border_color' => 'Border Color',
    'fill_color'   => 'Fill Color',
    );
  return $border_color_options;
}
