
---------------------------
PDF Stamper version 6.x-1.0
---------------------------
PDF Stamper is a dynamic PDF stamping module. It allows a user to upload
their pdf files into their private folder and allows them to stamp it with
custom text.

They can choose the colors, size, font and other parameters of the text
which needs to be stamped.



-----------------------------
DEPENDENCIES (Drupal Modules)
-----------------------------
PDF Stamper needs the following modules to function correctly. Please ensure they
are installed and configured *before* pdfstamper is installed.

- jQuery update (http://drupal.org/project/jquery_update)
  Please use 6.x-1.1.

- imageapi (http://drupal.org/project/imageapi)
  Please use 6.x-1.3+

  NOTE - One needs to enable either - ImageAPI GD2 or ImageAPI ImageMagick after enabling ImageAPI

  NOTE - If you are using ImageAPI Imagemagick (preferable) please ensure the path to imagemagick's convert is correct
         check the settings at http://www.example1.com/admin/settings/imageapi/config

- token (http://drupal.org/project/token)
  Please use 6.x-1.11+

- cck (http://drupal.org/project/cck)
  Please use 6.x-2.1+

  Also, ensure that Content Copy module is enabled.


- filefield (http://drupal.org/project/filefield)
  Please use 	6.x-3.0-beta1+



---------------------
EXTERNAL DEPENDENCIES
---------------------
Please read the file "README_EXTERNAL_LIB_PLUGIN.txt" as mentioned in step 6 of INSTALLATION

** IMPORTANT **

Please note - These libraries are not bundled and one has to install them separately and they will
be governed by their respective Licenses. The developers of this module (Venuslabs Web Solutions)
are not responsible for any violations that are done by the end user downloading and using this
module.

The end user using this module is therefore governed by the license of this module (GPL2) and
licences of the respective downloaded libraries

***************

------------
INSTALLATION
------------

1) Please ensure you have the files directory present and writable. To check the status for files directory
   goto - http://www.example.com/admin/logs/status and see the status of Filesystem. If you see
   ------------------------------------------------------------
   File system	Not writable
   The directory files does not exist. You may need to set the correct directory at the file system settings page or change the current directory's permissions so that it is writable.
   ------------------------------------------------------------
   Please create the files directory and make sure it's writable by webserver process.


2) Please install all the modules mentioned in DEPENDENCIES correctly.


3) Ensure Imageapi 6.x-1.3+ (http://drupal.org/project/imageapi) is installed and configured
   to use imagemagick(preferable) or gd2 toolkit (imagemagick is needed to get the preview
   images of the actual pdf document and gd2 can only generate a sample image of the pdf
   document size but not the content)

   For ImageMagick we've noticed in some cases the pdf preview image is not generated, this happens
   when the ImageAPI ImageMagick is installed for the first time and the settings have not been saved

   One can do the following to ensure that the settings for ImageAPI ImageMagick are saved once after
   installing the PDF Stamper.

   Goto - http://www.example1.com/admin/settings/imageapi/config
   Check the location of the 'convert' binary and other settings
   Click on 'Save Configuration' (click even if you've not changed anything)

   *NOTE* - One needs to enable either ImageAPI GD2 or ImageAPI ImageMagick after enabling ImageAPI.
          - Filefield depends on MimeDetect http://drupal.org/project/mimedetect



4) Place this module directory in your contributed modules folder (this will usually be
   "sites/all/modules/contrib" or "sites/all/modules/").


5) Install all the required external libraries and jquery plugin as mentioned the file "README_EXTERNAL_LIB_PLUGIN.txt"

6) Enable the module.
   When you enable it, you'll see the following status messages

   # The content fields table content_type_pdfstamper_pdf_file has been created.
   # The content type PDF File has been added.
   # Created field PDF Upload.
   # Saved field PDF Upload.
   # The configuration options have been saved.



7) Provide access control as per your needs for the content type - "pdfstamper_pdf_file"


----------------------------
Installation troubleshooting
----------------------------
If your pdfstamper does not work you must check if all files are extracted correctly. 
The directory ../modules/pdfstamper/ should have the following file structure:

 modules
   |
   |
   |-- pdfstamper
          |
          |--- pdfstamper.info
          |--- pdfstamper.module
          |--- pdfstamper.install
          |--- pdfstamper.inc
          |--- pdfstamper.css
          |--- pdf-preview.png
          |--- stamp.jpg
          |--- README.txt
          |--- js
          |     |
          |     |-- pdfstamper.js
          |     |
          |     |-- imgareaselect-0.4.2
          |     |        |
          |     |        |--- GPL-LICENSE.txt
          |     |        |--- jquery.imgareaselect-0.4.2.js
          |     |        |--- jquery.imgareaselect-0.4.2.min.js
          |     |        |--- MIT-LICENSE.txt
          |     |
          |     |-- jquery.imgareaselect.js -> imgareaselect-0.4.2/jquery.imgareaselect-0.4.2.min.js(create symlink manually)
          |
          |--- lib
                |
                |--  FPDF
                |      |
                |      |--- doc 
                |      |--- FAQ.htm 
                |      |--- font 
                |      |--- fpdf.css 
                |      |--- fpdf.php 
                |      |--- histo.htm 
                |      |--- install.txt 
                |      |--- tutorial 
                |
                |--  FPDF_TPL
                |      |
                |      |--- fpdf_tpl.php
                | 
                |--  FPDI
                       |
                       |--- decoders
                       |--- fpdi_pdf_parser.php
                       |--- fpdi.php
                       |--- pdf_context.php
                       |--- pdf_parser.php
                       |--- wrapper_functions.php


----------
USER GUIDE
----------

1 - Click on My PDF Documents (http://www.example1.com/pdfstamper/1) to see your existing PDF docs. Choose to stamp an existing one or upload a new PDF document.
2 - When one clicks on the Stamp icon the form for stamping the pdf is displayed
3 - After form is submitted it's taken to the My PDF Documents page (http://www.example1.com/pdfstamper/1/mypdf)
    and in the Drupal Message Bar the link for downloading the saved document is present.
    Click on the link 'Click here to download PDF' and download it.



Sometimes while saving the PDF and using ImageMagick convert to create the pdf document preview image one may see the errors

"ImageMagick reported an error:
**** Warning: Fonts with Subtype = /TrueType should be embedded. But Verdana-Bold is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But Verdana is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But ArialMT is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But TimesNewRomanPSMT is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But Verdana-Bold is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But Verdana is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But ArialMT is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But TimesNewRomanPSMT is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But Verdana is not embedded.
**** Warning: Fonts with Subtype = /TrueType should be embedded. But TimesNewRomanPSMT is not embedded.
**** This file had errors that were repaired or ignored.
**** The file was produced by:
**** >>>> Acrobat Distiller 8.0.0 (Windows) <<<<
**** Please notify the author of the software that produced this
**** file that it does not conform to Adobe's published PDF
**** specification."

The above error is due to a missing font family. We've tested in on Fedora Core 6 and inspite of the error the preview is generated just fine :).




-------
CREDITS
-------
The authors wish to thank (in alphabetical order) for usage of the respective libraries and plugins
Olivier Plathey - FPDF
Setasign - Jan Slabon - FPDI
Micha�� Wojciechowski - imgAreaSelect


-----
NOTES
-----
The code for the My PDF Documents section was inspired and reused from the Blog module


-----------
ASSUMPTIONS
-----------
1 - The monitor resolution is 72 ppi. This is needed to generate the preview image of the pdf document.
    (http://en.wikipedia.org/wiki/Pixels_per_inch)

