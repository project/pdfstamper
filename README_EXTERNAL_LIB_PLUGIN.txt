
----------------------------------------------
EXTERNAL LIBRARIES AND JQUERY PLUGINS REQUIRED
----------------------------------------------

*******************************
FPDF 1.53+ (http://www.fpdf.org)
*******************************

------------
INSTALLATION
------------
1) Download The latest version of the FPDF library from http://fpdf.org/en/download.php .

2) Please install the library in the lib directory of the pdfstamper module.

3) Rename the folder to FPDF

$ cd $drupal_root/path/to/pdfstamper_module/lib
$ tar -xzf fpdf16.tgz
$ mv fpdf16 FPDF


---------------------------------------------------------------------------------------------------------------

*****************************************************************
FPDI 1.2+ (http://www.setasign.de/products/pdf-php-solutions/fpdi)
*****************************************************************

------------
INSTALLATION
------------
1) Download The latest version of the FPDI library from http://www.setasign.de/products/pdf-php-solutions/fpdi/downloads/

2) Please create a folder called FPDI in the lib directory of the pdfstamper module and the install the library in that folder .

3) Go back into the FPDI directory(i.e cd FPDI)

4) Make sure there are read permissions to all the files present in the directory . If not change the permissions of the files to read only (i.e chmod -R a+r *)

$ cd $drupal_root/path/to/pdfstamper_module/lib
$ mkdir FPDI
$ cd FPDI
$ wget http://www.setasign.de/supra/kon2_dl/14195/FPDI-1.2.1.tgz
$ tar -xzf FPDI-1.2.1.tgz
$ chmod -R a+r *

----------------------------------------------------------------------------------------------------------------

*****************************************************************************
FPDF_TPL 1.1.1+ (http://www.setasign.de/support/manuals/fpdf-tpl/introduction)
*****************************************************************************

------------
INSTALLATION
------------

1) Download The latest version of the FPDF_TPL library from http://www.setasign.de/products/pdf-php-solutions/fpdi/downloads/

2) Please create a folder called FPDF_TPL in the lib directory of the pdfstamper module and the install the library in that folder .

3) Go back into the FPDF_TPL directory(i.e cd FPDF_TPL)

4) Make sure there are read permissions to all the files present in the directory . If not change the permissions of the files to read only (i.e chmod a+r *)

$ cd $drupal_root/path/to/pdfstamper_module/lib
$ mkdir FPDF_TPL
$ cd FPDF_TPL
$ wget http://www.setasign.de/supra/kon2_dl/14193/FPDF_TPL-1.1.2.tgz
$ tar -xzf FPDF_TPL-1.1.2.tgz
$ chmod -R a+r *

---------------------------------------------------------------------------------------------------------------------




***************************************************************
imgAreaSelect 0.4.2+ (http://odyniec.net/projects/imgareaselect)
***************************************************************

------------
INSTALLATION
------------
1) Download the latest version of imageareaselect from http://odyniec.net/projects/imgareaselect/

2) Please install the plugin in the js directory of the pdfstamper module.

3) Now we have to create a symlink to the imageareaselect js file
   In the pdfstamper js folder

$ cd $drupal_root/path/to/pdfstamper_module/js
$ wget http://odyniec.net/projects/imgareaselect/imgareaselect-0.6.1.zip
$ unzip imgareaselect-0.6.1.zip
$ ln -s imgareaselect-0.6.1/jquery.imgareaselect-0.6.1.min.js jquery.imgareaselect.js

   Linux/Unix Users
   ----------------
   ln -s imgareaselect-version/jquery.imgareaselect-version.min.js jquery.imgareaselect.js

   Window Users
   ------------

    For Windows Vista
    -----------------

    mklink /D  jquery.imgareaselect.js imgareaselect-version/jquery.imgareaselect-version.min.js

    For more information refer http://www.howtogeek.com/howto/windows-vista/using-symlinks-in-windows-vista/


---------------------------------------------------------------------------------------------------------------------

